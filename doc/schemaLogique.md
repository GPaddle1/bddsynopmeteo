# Structure général

Les différents univers sont regroupés dans un classe nommée `<NomDeL'univers>Data` afin de faciliter l'écriture de la classe EntryPoint.

# EntryPoint

- entryPoint

entryPoint(<ins>idEntryPoint</ins>, dateEntryPoint, #idStation, #idPrecipitationData, #idWindData, #idGeneralMetaData, #idSnowData, #idHumidityData, #idCloudData, #idPressureData, #idTemperatureData, #idWeatherDescription)

------

# Unit

- unit

unit(<ins>idUnit</ins>, unitName)

------

# Universe

- universe

universe(<ins>idUniverse</ins>, universeName)

------

# Barometric description

- barometricDesciption

barometricDesciption(<ins>idBarometricDesciption</ins>, barometricDescriptionCode, barometricDescription)

------

# Parameters description

- parametersDescription

parametersDescription(<ins>idParametersDescription</ins>, textualDescription)

------

# Weather description

- weatherDescription

weatherDescription(<ins>idWeatherDescription</ins>,weatherDescriptionCode, textualDescription)

------

# Station

- station

station(<ins>idStation</ins>, stationCode, stationName, lat, lng, altitude, #idRegion)

------

# Regions

- Regions

region(<ins>idRegion</ins>, regionName)
neighboursRegion(<ins>idRegion1, idRegion2</ins>)

------

# Precipitation

- precipitationData
- lastNHoursPrecipitation

precipitationData(<ins>idPrecipitationData</ins>, #idLast1HoursPrecipitation, #idLast3HoursPrecipitation, #idLast6HoursPrecipitation, #idLast12HoursPrecipitation, #idLast24HoursPrecipitation)

lastNHoursPrecipitation(<ins>idLastNHoursPrecipitation</ins>, paramValue, timeInterval, #idUniverse, #idUnit)

------

# Wind

- windData
- averageWindSpeed
- averageWindDirection
- windGustMeasurementPeriod
- windGustOnAPeriod
- windDirectionDescription
- windSpeedDescription

windData(<ins>idWindData</ins>, #idAverageWindSpeed, #idAverageWindDirection, #idWindGustMeasurementPeriod , #idWindGustOnAPeriod)

averageWindSpeed(<ins>idAverageWindSpeed</ins>, paramValue, #idUniverse, #idUnit, #idWindSpeedDescription)

averageWindDirection(<ins>idAverageWindDirection</ins>, paramValue, #idUniverse, #idUnit, #idWindDirectionDescription)

windGustMeasurementPeriod(<ins>idWindGustMeasurementPeriod </ins>, paramValue, #idUniverse, #idUnit)

windGustOnAPeriod(<ins>idWindGustOnAPeriod</ins>, paramValue, #idUniverse, #idUnit)

windDirectionDescription(<ins>idWindDirectionDescription</ins>, minValue, mxValue, textualDescription)

windSpeedDescription(<ins>idWindSpeedDescription</ins>, minValue, mxValue, textualDescription)

------

# General Metadata

- generalData
- horizontalVisibility
- dewPoint

generalData(<ins>idGeneralData</ins>, #idHorizontalVisibility, #idDewPoint)

horizontalVisibility(<ins>idHorizontalVisibility</ins>, paramValue, #idUniverse, #idUnit)

dewPoint(<ins>idDewPoint</ins>, paramValue, #idUniverse, #idUnit)

------

# Snow

- snowData
- snowHeight

snowData(<ins>idSnowData</ins>, #idSnowHeight)

snowHeight(<ins>idSnowHeight</ins>, paramValue, #idUniverse, #idUnit)

------

# Humidity

- humidityData
- humidity

humidityData(<ins>idHumidityData</ins>, #idHumidity)

humidity(<ins>idHumidity</ins>, paramValue, #idUniverse, #idUnit)

------

# Cloud

- cloudData
- lowCloudLevel

cloudData(<ins>idCloudData</ins>, #idLowCloudLevel)

lowCloudLevel(<ins>idLowCloudLevel</ins>, paramValue, #idUniverse, #idUnit)

------

# Pressure

- pressureData
- nHoursPressureVariation
- stationPressure
- seaLevelPressure

pressureData(<ins>idPressureData</ins>, #id1HoursPressureVariation, #id24HoursPressureVariation, #idStationPressure, #idSeaLevelPressure)

nHoursPressureVariation(<ins>idNHoursPressureVariation</ins>, paramValue, timeInterval, #idUniverse, #idUnit)

stationPressure(<ins>idStationPressure</ins>, paramValue, #idUniverse, #idUnit)

seaLevelPressure(<ins>idSeaLevelPressure</ins>, paramValue, #idUniverse, #idUnit)

------

# Temperature

- temperatureData
- temperature
- groundMinTemperature

temperatureData(<ins>idTemperatureData</ins>, #idTemperature, #idGroundMinTemperature)

temperature(<ins>idTemperature</ins>, paramValue, #idUniverse, #idUnit)

groundMinTemperature(<ins>idGroundMinTemperature</ins>, paramValue, #idUniverse, #idUnit)