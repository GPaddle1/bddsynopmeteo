# TP Météo

[[_TOC_]]

## Sujet 📜

Le sujet est disponible [à cette adresse](./TPMeteo.pdf)

## Etude préalable 🔎

Afin de choisir les données primordiales à placer dans la base de données, il est intéressant de regarder la quantité de données manquantes dans chaque catégorie ainsi on obtient les deux tableaux suivants :

<details>
<summary>Tableau n°1</summary>

| Descriptif | Mnémonique | Type | Unité | Données manquantes |
| --- | --- | --- | --- | --- |
| Méthode mesure tw | sw | int | code (3855) | 60 |
| Température du thermomètre mouillé | tw | réel | K | 60 |
| Phénomène spécial | phenspe1 | réel | code (3778) | 60 |
| Phénomène spécial | phenspe2 | réel | code (3778) | 60 |
| Phénomène spécial | phenspe3 | réel | code (3778) | 60 |
| Phénomène spécial | phenspe4 | réel | code (3778) | 60 |
| Type nuage 3 | ctype3 | int | code (0500) | 60 |
| Nébulosité cche nuageuse 4 | nnuage4 | int | octa | 60 |
| Type nuage 4 | ctype4 | int | code (0500) | 60 |
| Hauteur de base 4 | hnuage4 | int | m | 60 |
| Niveau barométrique | niv_bar | int | Pa | 59 |
| Géopotentiel | geop | int | m2/s2 | 59 |
| Température maximale sur 12 heures | tx12 | réel | K | 59 |
| Nébulosité cche nuageuse 3 | nnuage3 | int | octa | 59 |
| Hauteur de base 3 | hnuage3 | int | m | 59 |
| Type nuage 2 | ctype2 | int | code (0500) | 58 |
| Température minimale sur 12 heures | tn12 | réel | K | 57 |
| Hauteur de la neige fraîche | ssfrai | réel | m | 57 |
| Periode de mesure de la neige fraiche | perssfrai | réel | 1/10 heure | 57 |
| Temps passé 2 | w2 | int | code (4561) | 56 |
| Nébulosité cche nuageuse 2 | nnuage2 | int | octa | 56 |
| Hauteur de base 2 | hnuage2 | int | m | 56 |
| Type des nuages de l'étage supérieur | ch | int | code (0509) | 54 |
| Type nuage 1 | ctype1 | int | code (0500) | 54 |
| Type des nuages de l'étage moyen | cm | int | code (0515) | 52 |
| Temps passé 1 | w1 | int | code (4561) | 51 |
| Type des nuages de l'étage inférieur | cl | int | code (0513) | 51 |
| Température minimale sur 24 heures | tn24 | réel | K | 49 |
| Nebulosité totale | n | réel | % | 48 |
| Hauteur de la base des nuages de l'étage inférieur | hbas | int | mètre | 48 |
| Température maximale sur 24 heures | tx24 | réel | K | 48 |
| Nébulosité cche nuageuse 1 | nnuage1 | int | octa | 48 |
| Hauteur de base 1 | hnuage1 | int | m | 48 |
| Etat du sol | etat_sol | int | code (0901) | 33 |

</details>

<details>
<summary>Tableau n°2</summary>

| Descriptif | Mnémonique | Type | Unité | Données manquantes |
| --- | --- | --- | --- | --- |
| Hauteur totale de la couche de neige, glace, autre au sol | ht_neige | réel | m | 27 |
| Température minimale du sol sur 12 heures | tminsol | réel | K | 25 |
| Nébulosité des nuages de l'étage infrieur | nbas | int | octa | 18 |
| Temps présent | ww | int | [code (4677)](./data/Code4677/code4677.csv) | 15 |
| Rafales sur les 10 dernières minutes | raf10 | réel | m/s | 15 |
| Visibilité horizontale | vv | réel | m | 14 |
| Rafales sur une période | rafper | réel | m/s | 13 |
| Période de mesure de la rafale | per | réel | min | 13 |
| Précipitations dans les 24 dernières heures | rr24 | réel | mm | 6 |
| Précipitations dans les 12 dernières heures | rr12 | réel | mm | 4 |
| Pression au niveau mer | pmer | int | Pa | 3 |
| Variation de pression en 24 heures | tend24 | int | Pa | 3 |
| Précipitations dans les 3 dernières heures | rr3 | réel | mm | 3 |
| Précipitations dans les 1 dernières heures | rr1 | réel | mm | 2 |
| Précipitations dans les 6 dernières heures | rr6 | réel | mm | 2 |
| Variation de pression en 3 heures | tend | int | Pa | 1 |
| Type de tendance barométrique | cod_tend | int | [code (0200)](./data/Code0200/code0200.csv) | 1 |
| Température | t | réel | K | 1 |
| Point de rosée | td | réel | K | 1 |
| Humidité | u | int | % | 1 |
| Pression station | pres | int | Pa | 1 |
| Indicatif OMM station | numer_sta | car |  | 0 |
| Date (UTC) | date | car | AAAAMMDDHHMISS | 0 |
| Direction du vent moyen 10 mn | dd | int | degré | 0 |
| Vitesse du vent moyen 10 mn | ff | réel | m/s | 0 |

</details>


> Nous étudions ici un [export datant du 01/04 à 12h UTC](./data/synop.2021040112.csv), l'échantillon donne les informations sur 60 sites

Le premier est constitué des données qui ne seront pas retenues pour l'étude d'intérêt des données, celles-ci n'étant que trop peu présentes pour donner des informations sur les conditions réelles.

Le deuxième tableau présente des données ayant au-moins 50% de présence.

--------

## Schéma conceptuel 📊

### Univers définis

Afin de séparer les données les univers suivants ont été définis :

- Cloud
- GeneralMetadata
- Global
- Humidity
- Precipitation
- Pressure
- Snow
- Temperature
- Wind
- Description textuelle du temps [code (4677)](./data/Code4677/code4677.csv)
- Description textuelle de la tendance barométrique [code (0200)](./data/Code0200/code0200.csv)


Les données sont liées à leur univers afin de pouvoir les retrouver facilement par la suite. Par exemple, l'accès aux données sur le vent pourra se faire rapidement à l'aide de la clé relative à l'univers recherché.

### Modélisation

[![Modèle](./doc/Model.svg)](./doc/Model.svg)

Le modèle est disponible en résolution supérieur en cliquant dessus.

Il est important pour ne pas surcharger les différents appels de séparer selon différents "univers" les données traitées (Les informations sur le vent ne doivent pas appeler les données sur les précipitations à chaque fois par exemple).

Ainsi un objet d'entrée est défini permettant selon la date et la station d'avoir des informations à propos de différents univers.

La station est elle définie par son nom et son code et sa localisation.

La classe "Global" sert de point d'entrée et connait des informations sur l'ensemble des données définies plus bas (Par soucis de lisibilité du schéma, les associations sont données vers les univers). Toutes les associations sont alors de la forme `1..1` en dehors des tables ayant le champ timeInterval, celles si sont de la forme `1..*` car pouvant présenter les données sur 1h, 3h, 6h, ... pour une même station à un même instant.

Enfin, les tables sont toutes liées à la table des unités afin de conserver l'unicité de la notation de celles-ci. Dès lors il est possible de relier un instant à une valeur d'étude et son unité selon un univers par exemple.

### Piste d'amélioration

Il sera possible par la suite de remplacer la table `weatherDescription` par une table qui implémentera une logique de traduction. De cette façon, le champ de valeur sera remplacé par un identifiant qui, couplé à un indice de langue, permettra d'internationaliser la base de données.

### Schéma logique

Le schéma logique de la base de données est disponible [à cette addresse](./doc/schemaLogique.md)

--------

## Utilisation des scripts

Initialement, le travail a été effectué sur une machine Docker.
Pour exécuter les insertions il est important de conserver la structure interne du répertoire : 

```
- TPMeteo/
	- data/
	- scripts/
```

Au moment de la connection à la base de données il faut ajouter `--local-infile=1` (Comme présenté ci-dessous) ceci permettra d'utiliser les commandes d'insertion de données.

`mysql -u<username> -p<password> <database> --local-infile=1`

Par la suite les scripts pourront être lancés à l'aide de la commande `source <NomDuScript.sql>`

### Création des tables

La création des tables s'effectue à l'aide du [script CreateTables.sql](./scripts/CreateTables.sql)

`source CreateTables.sql`

--------

### Insertion des données

L'insertion des données s'effectue à l'aide du [script InsertData.sql](./scripts/InsertData.sql)

`source InsertData.sql`

--------

### Exécution des requêtes

L'exécution des requêtes s'effectue à l'aide du [script Requetes.sql](./scripts/Requetes.sql)

`source Requetes.sql`

--------

### Exécution des procédures

L'exécution des procédures s'effectue à l'aide du [script Procedures.sql](./scripts/Procedures.sql)

`source Procedures.sql`

--------

## Résumé 

Dans l'ordre les commandes sont les suivantes : 

```bash
	git clone https://gitlab.com/GPaddle1/bddsynopmeteo.git
	cd scripts
	mysql -u<username> -p<password> <database> --local-infile=1

	source CreateTables.sql
	source InsertData.sql
	source Requetes.sql
	source Procedures.sql
```

## Crédits 🖋

Keller Guillaume @EI-CNAM FIP1A 2020-2021