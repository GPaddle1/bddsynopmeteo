Drop Table If Exists parametersDescription;

Drop Table If Exists neighboursRegion;

-- Entry point tables
Drop Table If Exists entryPoint;

Drop Table If Exists station;

Drop Table If Exists region;

-- Data link tables 
Drop Table If Exists precipitationData;

Drop Table If Exists windData;

Drop Table If Exists generalData;

Drop Table If Exists snowData;

Drop Table If Exists humidityData;

Drop Table If Exists cloudData;

Drop Table If Exists pressureData;

Drop Table If Exists temperatureData;

-- Data tables
Drop Table If Exists lastNHoursPrecipitation;

Drop Table If Exists averageWindSpeed;

Drop Table If Exists averageWindDirection;

Drop Table If Exists windGustMeasurementPeriod;

Drop Table If Exists windGustOnAPeriod;

Drop Table If Exists windDirectionDescription;

Drop Table If Exists windSpeedDescription;

Drop Table If Exists horizontalVisibility;

Drop Table If Exists dewPoint;

Drop Table If Exists snowHeight;

Drop Table If Exists humidity;

Drop Table If Exists lowCloudLevel;

Drop Table If Exists nHoursPressureVariation;

Drop Table If Exists stationPressure;

Drop Table If Exists seaLevelPressure;

Drop Table If Exists temperature;

Drop Table If Exists groundMinTemperature;

-- General data tables
Drop Table If Exists unit;

Drop Table If Exists universe;

Drop Table If Exists barometricDescription;

Drop Table If Exists weatherDescription;
