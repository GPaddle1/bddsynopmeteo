-- a) Créez une procédure stockée qui à partir d’un N° de station et
-- une date/heure de début et une date/heure de fin renvoie l’ensemble
-- des températures dans cet intervalle;

DROP PROCEDURE IF Exists getTempByDateAndStation;

DELIMITER |
CREATE PROCEDURE getTempByDateAndStation (IN numStation INT, IN startDate VARCHAR(14), IN endDate VARCHAR(14)) 
	BEGIN
		select
			station.stationName,
			CONCAT(
				temperature.paramValue,
				" ",
				unit.unitName) as Temperature,
			dateEntryPoint 
		from
			temperature
			inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
			inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
			inner join station on station.idStation = entryPoint.idStation
			inner join unit on temperature.idUnit = unit.idUnit
		where
			station.stationCode = numStation
			AND dateEntryPoint >= STR_TO_DATE(startDate, '%Y%m%d%H%i%S')
			AND dateEntryPoint <= STR_TO_DATE(endDate, '%Y%m%d%H%i%S');
	END
	|

delimiter ;
call getTempByDateAndStation(7005, "20210301000000", "20210330000000");

 
 -- b) Idem pour la pression;

DROP PROCEDURE IF Exists getPressureByDateAndStation;

DELIMITER |
CREATE PROCEDURE getPressureByDateAndStation (IN numStation INT, IN startDate VARCHAR(14), IN endDate VARCHAR(14)) 
	BEGIN
		select
			station.stationName,
			CONCAT(
				stationPressure.paramValue,
				" ",
				unit.unitName) as Pression,
			dateEntryPoint
		from
			stationPressure
			inner join pressureData on stationPressure.idStationPressure = pressureData.idStationPressure
			inner join entryPoint on entryPoint.idPressureData = pressureData.idPressureData
			inner join station on station.idStation = entryPoint.idStation
			inner join unit on stationPressure.idUnit = unit.idUnit
		where
			station.stationCode = numStation
			AND dateEntryPoint >= STR_TO_DATE(startDate, '%Y%m%d%H%i%S')
			AND dateEntryPoint <= STR_TO_DATE(endDate, '%Y%m%d%H%i%S');
	END
	|

delimiter ;
call getPressureByDateAndStation(7005, "20210301000000", "20210330000000");

 -- c) Idem pour Tmax et Tmin;

 
DROP PROCEDURE IF Exists getMinMaxTemperatureByDateAndStation;

DELIMITER |
CREATE PROCEDURE getMinMaxTemperatureByDateAndStation (IN numStation INT, IN startDate VARCHAR(14), IN endDate VARCHAR(14)) 
	BEGIN
		select
			station.stationName,
			CONCAT(
				min(temperature.paramValue),
				" ",
				unit.unitName) as minTemperature,
			CONCAT(
				max(temperature.paramValue),
				" ",
				unit.unitName) as maxTemperature
		from
			temperature
			inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
			inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
			inner join station on station.idStation = entryPoint.idStation
			inner join unit on temperature.idUnit = unit.idUnit
		where
			station.stationCode = numStation
			AND dateEntryPoint >= STR_TO_DATE(startDate, '%Y%m%d%H%i%S')
			AND dateEntryPoint <= STR_TO_DATE(endDate, '%Y%m%d%H%i%S')
		group by
			station.stationName,
			unit.unitName;
	END
	|

delimiter ;
call getMinMaxTemperatureByDateAndStation(7005, "20210301000000", "20210330000000");


 -- d) Idem pour Pmax et Pmin;

DROP PROCEDURE IF Exists getMinMaxPressureByDateAndStation;

DELIMITER |
CREATE PROCEDURE getMinMaxPressureByDateAndStation (IN numStation INT, IN startDate VARCHAR(14), IN endDate VARCHAR(14)) 
	BEGIN
		select
			station.stationName,
			CONCAT(
				min(stationPressure.paramValue),
				" ",
				unit.unitName) as minPressure,
			CONCAT(
				max(stationPressure.paramValue),
				" ",
				unit.unitName) as maxPressure
		from
			stationPressure
			inner join pressureData on stationPressure.idStationPressure = pressureData.idStationPressure
			inner join entryPoint on entryPoint.idPressureData = pressureData.idPressureData
			inner join station on station.idStation = entryPoint.idStation
			inner join unit on stationPressure.idUnit = unit.idUnit
		where
			station.stationCode = numStation
			AND dateEntryPoint >= STR_TO_DATE(startDate, '%Y%m%d%H%i%S')
			AND dateEntryPoint <= STR_TO_DATE(endDate, '%Y%m%d%H%i%S')
		group by
			station.stationName,
			unit.unitName;
	END
	|

delimiter ;
call getMinMaxPressureByDateAndStation(7005, "20210301000000", "20210330000000");

delimiter ;