SET GLOBAL local_infile=1;

-- Description tables

delete from weatherDescription;
LOAD DATA LOCAL INFILE '../data/CodeTempsPresent.csv' 
	INTO TABLE weatherDescription
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(idWeatherDescription, weatherDescriptionCode, textualDescription);

-- select * from weatherDescription;

delete from barometricDescription;
LOAD DATA LOCAL INFILE '../data/CodeTend.csv' 
	INTO TABLE barometricDescription
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@IDCodeBaro, @Code, @TextualDescription)
	SET idBarometricDescription = @IDCodeBaro, barometricDescriptionCode = @Code, barometricDescription = @TextualDescription;

-- select * from barometricDescription;

delete from windDirectionDescription;
LOAD DATA LOCAL INFILE '../data/windDirectionDescription.csv' 
	INTO TABLE windDirectionDescription
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@ID, @Description, @angleMinValue, @angleMaxValue)
	SET idWindDirectionDescription = @ID, minValue = @angleMinValue, mxValue = @angleMaxValue, textualDescription = @Description;

-- select * from windDirectionDescription;
	
delete from windSpeedDescription;
LOAD DATA LOCAL INFILE '../data/windSpeedDescription.csv' 
	INTO TABLE windSpeedDescription
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@idWindSpeedDescription, @BeaufortID, @Description, @msMinValue, @msMaxValue, @Observation)
	SET idWindSpeedDescription = @idWindSpeedDescription, minValue = @msMinValue, mxValue = @msMaxValue, textualDescription = @Description, observation = @Observation;

-- select * from windSpeedDescription;

-- Region table

delete from region;
LOAD DATA LOCAL INFILE '../data/Regions.csv' 
	INTO TABLE region
 	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(idRegion, regionName);

-- select * from region;

delete from neighboursRegion;
LOAD DATA LOCAL INFILE '../data/NeighboursRegions.csv' 
	INTO TABLE neighboursRegion 
 	FIELDS TERMINATED BY ";"
	 LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@idRegion1, @NomRegion1, @idRegion2, @NomRegion2)
	SET idRegion1 = @idRegion1, idRegion2 = @idRegion2;

-- select * from neighboursRegion;

-- Station table

delete from station;
LOAD DATA LOCAL INFILE '../data/Stations.csv' 
	INTO TABLE station 
 	FIELDS TERMINATED BY ";"
	 LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@idStation, @ID, @Nom, @Latitude, @Longitude, @Altitude, @idRegion)
	SET idStation = @idStation,stationCode = @ID,stationName = @Nom,lat = @Latitude,lng = @Longitude,altitude = @Altitude,idRegion = @idRegion;

-- select * from station;

-- Unit table

delete from unit;
LOAD DATA LOCAL INFILE '../data/Units.csv' 
	INTO TABLE unit
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES;

-- select * from unit;

-- Universe table

delete from universe;
LOAD DATA LOCAL INFILE '../data/universes.csv' 
	INTO TABLE universe
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@universeID, @universeName)
	SET idUniverse = @universeID, universeName = @universeName;

-- select * from universe;


delete from parametersDescription;
LOAD DATA LOCAL INFILE '../data/ParamSynop.csv' 
	INTO TABLE parametersDescription 
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@idParametersDescription, @Descriptif, @Mnemonique, @Type, @Unite)
	SET idParametersDescription = @idParametersDescription, textualDescription = @Descriptif;

-- select * from parametersDescription;

-- Datas

-- Precipitation

delete from lastNHoursPrecipitation;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE lastNHoursPrecipitation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per, @etat_sol, @ht_neigeUnitID, @ht_neige, @ssfrai, @perssfrai, @rr1UnitID, @rr1)
	SET idLastNHoursPrecipitation=(@id * 5 + 0), paramValue = @rr1, timeInterval = 1,idUniverse=2, idUnit = @rr1UnitID;


LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE lastNHoursPrecipitation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per, @etat_sol, @ht_neigeUnitID, @ht_neige, @ssfrai, @perssfrai, @rr1UnitID, @rr1, @rr3UnitID, @rr3)
	SET idLastNHoursPrecipitation=(@id * 5 + 1), paramValue = @rr3, timeInterval = 3,idUniverse=2, idUnit = @rr3UnitID;


LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE lastNHoursPrecipitation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per, @etat_sol, @ht_neigeUnitID, @ht_neige, @ssfrai, @perssfrai, @rr1UnitID, @rr1, @rr3UnitID, @rr3, @rr6UnitID, @rr6)
	SET idLastNHoursPrecipitation=(@id * 5 + 2), paramValue = @rr6, timeInterval = 6,idUniverse=2, idUnit = @rr6UnitID;


LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE lastNHoursPrecipitation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per, @etat_sol, @ht_neigeUnitID, @ht_neige, @ssfrai, @perssfrai, @rr1UnitID, @rr1, @rr3UnitID, @rr3, @rr6UnitID, @rr6, @rr12UnitID, @rr12)
	SET idLastNHoursPrecipitation=(@id * 5 + 3), paramValue = @rr12, timeInterval = 12,idUniverse=2, idUnit = @rr12UnitID;


LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE lastNHoursPrecipitation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per, @etat_sol, @ht_neigeUnitID, @ht_neige, @ssfrai, @perssfrai, @rr1UnitID, @rr1, @rr3UnitID, @rr3, @rr6UnitID, @rr6, @rr12UnitID, @rr12, @rr24UnitID, @rr24)
	SET idLastNHoursPrecipitation=(@id * 5 + 4), paramValue = @rr24, timeInterval = 24,idUniverse=2, idUnit = @rr24UnitID;

-- select * from lastNHoursPrecipitation;

delete from precipitationData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE precipitationData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idPrecipitationData = @id, idLast1HoursPrecipitation=(@id * 5 ),idLast3HoursPrecipitation=(@id * 5 + 1),idLast6HoursPrecipitation=(@id * 5 + 2),idLast12HoursPrecipitation=(@id * 5 + 3),idLast24HoursPrecipitation=(@id * 5 )+4;

-- select * from precipitationData;

-- Wind

delete from averageWindSpeed;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE averageWindSpeed
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff)
	SET idAverageWindSpeed = @id, paramValue = @ff, idUniverse=1, idUnit = @ffUnitID, idWindSpeedDescription = (select idWindSpeedDescription from windSpeedDescription where minValue <= @ff AND mxValue > @ff);

-- select * from averageWindSpeed;

delete from averageWindDirection;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE averageWindDirection
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd)
	SET idAverageWindDirection = @id, paramValue = @dd, idUniverse=1, idUnit = @ddUnitID, idWindDirectionDescription = (select idWindSpeedDescription from windSpeedDescription where minValue <= @dd AND mxValue > @dd);

-- select * from averageWindDirection;

delete from windGustMeasurementPeriod;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE windGustMeasurementPeriod
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per)
	SET idWindGustMeasurementPeriod = @id, paramValue = @per, idUniverse=1, idUnit = @perUnitID;

-- select * from windGustMeasurementPeriod;

delete from windGustOnAPeriod;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE windGustOnAPeriod
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper)
	SET idWindGustOnAPeriod = @id, paramValue = @rafper, idUniverse = 1, idUnit = @rafperUnitID;

-- select * from windGustOnAPeriod;

delete from windData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE windData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idWindData = @id, idAverageWindSpeed = @id, idAverageWindDirection = @id, idWindGustMeasurementPeriod = @id, idWindGustOnAPeriod= @id;

-- select * from windData;

-- GeneralData

delete from horizontalVisibility;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE horizontalVisibility
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv)
	SET idHorizontalVisibility = @id, paramValue = @vv, idUniverse = 5, idUnit = @vvUnitID;

-- select * from horizontalVisibility;

delete from dewPoint;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE dewPoint
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td)
	SET idDewPoint = @id, paramValue = @td, idUniverse=5, idUnit = @tdUnitID;

-- select * from dewPoint;

delete from generalData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE generalData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idGeneralData = @id, idHorizontalVisibility = @id, idDewPoint = @id;

-- select * from generalData;

-- Snow

delete from snowHeight;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE snowHeight
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol, @sw, @tw, @raf10UnitID, @raf10, @rafperUnitID, @rafper, @perUnitID, @per, @etat_sol, @ht_neigeUnitID, @ht_neige)
	SET idSnowHeight = @id, paramValue = @ht_neige, idUniverse=3, idUnit = @ht_neigeUnitID;

-- select * from snowHeight;

delete from snowData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE snowData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idSnowData= @id, idSnowHeight= @id;

-- select * from snowData;

-- Humidity

delete from humidity;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE humidity
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u)
	SET idHumidity = @id, paramValue = @u, idUniverse = 4, idUnit = @uUnitID;

-- select * from humidity;

delete from humidityData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE humidityData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idHumidityData = @id, idHumidity = @id;

-- select * from humidityData;

-- Cloud

delete from lowCloudLevel;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE lowCloudLevel
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas)
	SET idLowCloudLevel = @id, paramValue = @hbas, idUniverse = 6, idUnit = @hbasUnitID;

-- select * from lowCloudLevel;

delete from cloudData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE cloudData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idCloudData = @id, idLowCloudLevel = @id;

-- select * from cloudData;

-- Pressure

delete from nHoursPressureVariation;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE nHoursPressureVariation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend)
	SET idNHoursPressureVariation = (@id * 2 + 0), paramValue = @tend, timeInterval = 3, idUniverse = 6, idUnit = @tendUnitID;

-- select * from nHoursPressureVariation;

LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE nHoursPressureVariation
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24)
	SET idNHoursPressureVariation = (@id * 2 + 1), paramValue = @tend24, timeInterval = 24, idUniverse = 6, idUnit = @tend24UnitID;

-- select * from nHoursPressureVariation;

delete from stationPressure;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE stationPressure
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres)
	SET idStationPressure = @id, paramValue = @pres, idUniverse = 6, idUnit = @presUnitID;

-- select * from stationPressure;

delete from seaLevelPressure;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE seaLevelPressure
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer)
	SET idSeaLevelPressure = @id, paramValue = @pmer, idUniverse = 6, idUnit = @pmerUnitID;

-- select * from seaLevelPressure;


delete from pressureData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE pressureData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idPressureData = @id, id1HoursPressureVariation = (@id * 2) + 0, id24HoursPressureVariation = (@id * 2) + 1, idStationPressure = @id, idSeaLevelPressure = @id;

-- select * from pressureData;

-- Temperature

delete from temperature;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE temperature
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t)
	SET idTemperature = @id, paramValue = @t, idUniverse = 7, idUnit = @tUnitID;

-- select * from temperature;

delete from groundMinTemperature;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv' 
	INTO TABLE groundMinTemperature
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww, @w1, @w2, @n, @nbasUnitID, @nbas, @hbasUnitID, @hbas, @cl, @cm, @ch, @presUnitID, @pres, @niv_bar, @geop, @tend24UnitID, @tend24, @tn12, @tn24, @tx12, @tx24, @tminsolUnitID, @tminsol)
	SET idGroundMinTemperature = @id, paramValue = @tminsol, idUniverse = 7, idUnit = @tminsolUnitID;

-- select * from groundMinTemperature;

delete from temperatureData;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv'
	INTO TABLE temperatureData
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id)
	SET idTemperatureData = @id, idTemperature = @id, idGroundMinTemperature = @id;

-- select * from temperatureData;

-- Entry Point

delete from entryPoint;
LOAD DATA LOCAL INFILE '../data/Synop_Mars.csv'
	INTO TABLE entryPoint
	FIELDS TERMINATED BY ";"
	LINES TERMINATED BY "\r\n"
	IGNORE 1 LINES
	(@id, @idStation, @numer_sta, @dateFormate, @date, @pmerUnitID, @pmer, @tendUnitID, @tend, @cod_tend, @ddUnitID, @dd, @ffUnitID, @ff, @tUnitID, @t, @tdUnitID, @td, @uUnitID, @u, @vvUnitID, @vv, @ww)
	SET idEntryPoint = @id, dateEntryPoint = @dateFormate, idStation = @idStation, idPrecipitationData = @id, idWindData = @id, idGeneralData = @id, idSnowData = @id, idHumidityData = @id, idCloudData = @id, idPressureData = @id, idTemperatureData = @id, idWeatherDescription = (@ww+1), idBarometricDescription = (@cod_tend+1);

-- select * from entryPoint;
