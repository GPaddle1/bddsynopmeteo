-- a) Donnez la liste de températures de la station n° 7005;
-- OK
select
	concat(temperature.paramValue, " ", unit.unitName) as Temperature
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join unit on temperature.idUnit = unit.idUnit
where
	station.stationCode = 7005;

-- b) Donnez Tmin, T max pour la station n° 7577 pour la date d’hier;
-- OK
select
	CONCAT(min(temperature.paramValue), " ", unit.unitName) as TemperatureMin,
	CONCAT(max(temperature.paramValue), " ", unit.unitName) as TemperatureMax
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join unit on temperature.idUnit = unit.idUnit
where
	station.stationCode = 7577
	AND DAY(dateEntryPoint) = 1
	AND MONTH(dateEntryPoint) = 3
GROUP BY
	unit.unitName;

-- c) Donnez la description du temps pour la station n°7643, pour le 20/10/2018 à 18H;
-- OK
select
	"Meteo pour le 1/3/2021 a 18h : " as DescriptionTemps,
	weatherDescription.textualDescription as DescriptionTemps
from
	weatherDescription
	inner join entryPoint on weatherDescription.idWeatherDescription = entryPoint.idWeatherDescription
	inner join station on station.idStation = entryPoint.idStation
where
	DAY(dateEntryPoint) = 1
	AND HOUR(dateEntryPoint) = 18
	AND station.stationCode = 7577;

-- d) Comptez le nombre de station dans la base;
-- OK
select
	CONCAT("Il y a ", count(*), " stations en base")
from
	station;

-- e) Donnez les températures et les pressions de la semaine pour la station de Strasbourg-Entzheim;
-- OK
select
	"Strasbourg-Entzheim",
	CONCAT(
		"Temperature : ",
		temperature.paramValue,
		" ",
		TUnit.unitName,
		" ",
		"Pression : ",
		stationPressure.paramValue,
		" ",
		PUnit.unitName
	) as dataSE,
	dateEntryPoint as dateMesure
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join pressureData on entryPoint.idPressureData = pressureData.idPressureData
	inner join stationPressure on stationPressure.idStationPressure = pressureData.idStationPressure
	inner join station on station.idStation = entryPoint.idStation
	inner join unit TUnit on temperature.idUnit = TUnit.idUnit
	inner join unit PUnit on stationPressure.idUnit = PUnit.idUnit
where
	station.stationName = 'Strasbourg-Entzheim'
	AND dateEntryPoint >= STR_TO_DATE('2021-03-02', '%Y-%m-%d')
	AND dateEntryPoint <= STR_TO_DATE('2021-03-09', '%Y-%m-%d');

-- f) Donnez la température moyenne de la semaine par n° de station;
-- OK
select
	CONCAT(avg(temperature.paramValue), " ", unit.unitName) as moyenneT,
	CONCAT(stationCode, " ", stationName) as dataStation
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join unit on temperature.idUnit = unit.idUnit
where
	dateEntryPoint >= STR_TO_DATE('2021-03-02', '%Y-%m-%d')
	AND dateEntryPoint <= STR_TO_DATE('2021-03-09', '%Y-%m-%d')
GROUP BY
	station.idStation,
	unit.unitName;

-- g) Donnez la température moyenne et la pression moyenne de la semaine par nom de station;
-- OK
select
	stationName,
	CONCAT(
		avg(temperature.paramValue),
		" ",
		TUnit.unitName
	) as moyTemperature,
	CONCAT(
		avg(stationPressure.paramValue),
		" ",
		PUnit.unitName
	) as moyPression
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join pressureData on entryPoint.idPressureData = pressureData.idPressureData
	inner join stationPressure on stationPressure.idStationPressure = pressureData.idStationPressure
	inner join station on station.idStation = entryPoint.idStation
	inner join unit TUnit on temperature.idUnit = TUnit.idUnit
	inner join unit PUnit on stationPressure.idUnit = PUnit.idUnit
where
	dateEntryPoint >= STR_TO_DATE('2021-03-02', '%Y-%m-%d')
	AND dateEntryPoint <= STR_TO_DATE('2021-03-09', '%Y-%m-%d')
group by
	station.idStation,
	TUnit.unitName,
	PUnit.unitName;

-- h) Donnez la date/heure, le nom de la station et la direction du vent en lettre pour l’ensemble des relevés;
-- OK
select
	dateEntryPoint,
	station.stationName,
	windDirectionDescription.textualDescription as wind
from
	windDirectionDescription
	inner join averageWindDirection on windDirectionDescription.idWindDirectionDescription = averageWindDirection.idWindDirectionDescription
	inner join windData on averageWindDirection.idAverageWindDirection = windData.idAverageWindDirection
	inner join entryPoint on entryPoint.idWindData = windData.idWindData
	inner join station on station.idStation = entryPoint.idStation;

-- i) Donnez la date et le nom de la station qui a des valeurs manquantes dans une des mesures
-- OK
select
	dateEntryPoint,
	station.stationName,
	CONCAT(
		IF (humidity.paramValue = 0, " - humidity", ""),
		IF (1HP.paramValue = 0, " - 1HP", ""),
		IF (24HP.paramValue = 0, " - 24HP", ""),
		IF (
			stationPressure.paramValue = 0,
			" - stationPressure",
			""
		),
		IF (
			seaLevelPressure.paramValue = 0,
			" - seaLevelPressure",
			""
		),
		IF (temperature.paramValue = 0, " - temperature", ""),
		IF (
			groundMinTemperature.paramValue = 0,
			" - groundMinTemperature",
			""
		),
		IF (
			averageWindSpeed.paramValue = 0,
			" - averageWindSpeed",
			""
		),
		IF (
			windGustMeasurementPeriod.paramValue = 0,
			" - windGustMeasurementPeriod",
			""
		),
		IF (
			windGustOnAPeriod.paramValue = 0,
			" - windGustOnAPeriod",
			""
		)
	)
from
	entryPoint
	/**/
	inner join humidityData on entryPoint.idHumidityData = humidityData.idHumidityData
	inner join humidity on humidity.idHumidity = humidityData.idHumidity
	/**/
	inner join pressureData on entryPoint.idPressureData = pressureData.idPressureData
	inner join nHoursPressureVariation 1HP on 1HP.idNHoursPressureVariation = pressureData.id1HoursPressureVariation
	inner join nHoursPressureVariation 24HP on 24HP.idNHoursPressureVariation = pressureData.id24HoursPressureVariation
	inner join stationPressure on stationPressure.idStationPressure = pressureData.idPressureData
	inner join seaLevelPressure on seaLevelPressure.idSeaLevelPressure = pressureData.idPressureData
	/**/
	inner join temperatureData on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join temperature on temperature.idTemperature = temperatureData.idTemperature
	inner join groundMinTemperature on groundMinTemperature.idGroundMinTemperature = temperatureData.idGroundMinTemperature
	/**/
	inner join windData on entryPoint.idWindData = windData.idWindData
	inner join averageWindSpeed on averageWindSpeed.idAverageWindSpeed = windData.idAverageWindSpeed
	inner join averageWindDirection on averageWindDirection.idAverageWindDirection = windData.idAverageWindDirection
	inner join windGustMeasurementPeriod on windGustMeasurementPeriod.idWindGustMeasurementPeriod = windData.idWindGustMeasurementPeriod
	inner join windGustOnAPeriod on windGustOnAPeriod.idWindGustOnAPeriod = windData.idWindGustOnAPeriod
	/**/
	inner join station on station.idStation = entryPoint.idStation
where
	/* humidityData*/
	humidity.paramValue = 0
	/* pressureData*/
	OR 1HP.paramValue = 0
	OR 24HP.paramValue = 0
	OR stationPressure.paramValue = 0
	OR seaLevelPressure.paramValue = 0
	/* temperatureData*/
	OR temperature.paramValue = 0
	OR groundMinTemperature.paramValue = 0
	/* windData*/
	OR averageWindSpeed.paramValue = 0
	OR windGustMeasurementPeriod.paramValue = 0
	OR windGustOnAPeriod.paramValue = 0;

-- j) Donnez la moyenne de température par région
-- OK
select
	CONCAT(
		"Temperature moyenne en ",
		region.regionName
	) as descriptionLieux,
	CONCAT(
		avg(temperature.paramValue),
		" ",
		unit.unitName
	) as descriptionTMoy
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join region on region.idRegion = station.idRegion
	inner join unit on temperature.idUnit = unit.idUnit
GROUP BY
	region.idRegion,
	unit.unitName;

-- k) Quelle est la région la plus chaude? la plus froide?
-- OK
select
	concat(
		"La region la plus chaude est ",
		regionName,
		" avec ",
		avg(temperature.paramValue),
		" ",
		unit.unitName
	) as description
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join region on region.idRegion = station.idRegion
	inner join unit on temperature.idUnit = unit.idUnit
group by
	region.idRegion,
	unit.unitName
having
	avg(temperature.paramValue) >= ALL (
		select
			avg(temperature.paramValue)
		from
			temperature
			inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
			inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
			inner join station on station.idStation = entryPoint.idStation
			inner join region on region.idRegion = station.idRegion
			inner join unit on temperature.idUnit = unit.idUnit
		GROUP BY
			region.idRegion
	);

select
	concat(
		"La region la plus froide est ",
		regionName,
		" avec ",
		avg(temperature.paramValue),
		" ",
		unit.unitName
	) as description
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join region on region.idRegion = station.idRegion
	inner join unit on temperature.idUnit = unit.idUnit
group by
	region.idRegion,
	unit.unitName
having
	avg(temperature.paramValue) <= ALL (
		select
			avg(temperature.paramValue)
		from
			temperature
			inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
			inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
			inner join station on station.idStation = entryPoint.idStation
			inner join region on region.idRegion = station.idRegion
			inner join unit on temperature.idUnit = unit.idUnit
		GROUP BY
			region.idRegion
	);

-- l) Donnez les régions dont la température moyenne est inférieure à la température moyenne nationale;
-- OK
select
	CONCAT (
		"La temperature moyenne de ",
		regionName,
		" est inferieure a la temperature moyenne nationale"
	) as DescriptionTMoy
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
	inner join region on region.idRegion = station.idRegion
GROUP BY
	region.idRegion
HAVING
	avg(temperature.paramValue) <= (
		select
			avg(temperature.paramValue)
		from
			temperature
	);

-- m) Donnez les régions limitrophes à la région Grand Est
-- OK
select
	concat(
		R2.regionName,
		" est voisine de la région Grand Est"
	) as Description
from
	neighboursRegion
	inner join region R1 on R1.idRegion = neighboursRegion.idRegion1
	inner join region R2 on R2.idRegion = neighboursRegion.idRegion2
where
	R1.regionName = "Grand est";

-- n) Quelle région à le plus de voisins?
-- OK
select
	CONCAT(
		regionName,
		" est la région avec le plus de voisins (",
		count(*),
		")"
	) as Descriptif
from
	region
	inner join neighboursRegion on region.idRegion = neighboursRegion.idRegion1
group by
	region.idRegion
having
	count(*) >= ALL (
		select
			count(*)
		from
			region
			inner join neighboursRegion on region.idRegion = neighboursRegion.idRegion1
		group by
			region.idRegion
	);

-- o) Y a t-il des station dont la température max est inférieur à la température moyenne nationale?
-- OK
select
	CONCAT(
		station.stationName,
		" a une temperature max (",
		max(temperature.paramValue),
		") < a la moyenne nationale"
	) as Description
from
	temperature
	inner join temperatureData on temperature.idTemperature = temperatureData.idTemperature
	inner join entryPoint on entryPoint.idTemperatureData = temperatureData.idTemperatureData
	inner join station on station.idStation = entryPoint.idStation
GROUP BY
	station.idStation
HAVING
	max(temperature.paramValue) <= (
		select
			avg(temperature.paramValue)
		from
			temperature
	);

-- p) Y a-t-il des stations où il a plu toute une journée?
-- OK
select
	IF(
		count(*) > 0,
		CONCAT(
			"Il existe ",
			count(*),
			" station(s) ou il a plu toute la journee le ",
			TTot.Jour
		),
		CONCAT(
			"Il n'existe pas de station ou il a plu toute la journee le ",
			TTot.Jour
		)
	) as Description
from
	(
		select
			station.stationName as Nomstation,
			DAY(dateEntryPoint) as Jour
		from
			entryPoint
			inner join station on entryPoint.idStation = station.idStation
			inner join precipitationData on precipitationData.idPrecipitationData = entryPoint.idPrecipitationData
			inner join lastNHoursPrecipitation on precipitationData.idLast24HoursPrecipitation = lastNHoursPrecipitation.idLastNHoursPrecipitation
		group by
			stationName,
			DAY(dateEntryPoint)
		having
			min(lastNHoursPrecipitation.paramValue) != 0
	) as TTot
group by
	TTot.Jour
ORDER BY
	TTot.Jour;