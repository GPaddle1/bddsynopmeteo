Drop Table If Exists parametersDescription;

Drop Table If Exists neighboursRegion;

-- Entry point tables
Drop Table If Exists entryPoint;

Drop Table If Exists station;

Drop Table If Exists region;

-- Data link tables 
Drop Table If Exists precipitationData;

Drop Table If Exists windData;

Drop Table If Exists generalData;

Drop Table If Exists snowData;

Drop Table If Exists humidityData;

Drop Table If Exists cloudData;

Drop Table If Exists pressureData;

Drop Table If Exists temperatureData;

-- Data tables
Drop Table If Exists lastNHoursPrecipitation;

Drop Table If Exists averageWindSpeed;

Drop Table If Exists averageWindDirection;

Drop Table If Exists windGustMeasurementPeriod;

Drop Table If Exists windGustOnAPeriod;

Drop Table If Exists windDirectionDescription;

Drop Table If Exists windSpeedDescription;

Drop Table If Exists horizontalVisibility;

Drop Table If Exists dewPoint;

Drop Table If Exists snowHeight;

Drop Table If Exists humidity;

Drop Table If Exists lowCloudLevel;

Drop Table If Exists nHoursPressureVariation;

Drop Table If Exists stationPressure;

Drop Table If Exists seaLevelPressure;

Drop Table If Exists temperature;

Drop Table If Exists groundMinTemperature;

-- General data tables
Drop Table If Exists unit;

Drop Table If Exists universe;

Drop Table If Exists barometricDescription;

Drop Table If Exists weatherDescription;

-- Tables creation
CREATE TABLE unit(
	idUnit Int Auto_Increment NOT NULL Primary Key,
	unitName VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE universe(
	idUniverse Int Auto_Increment NOT NULL Primary Key,
	universeName Enum(
		"Wind",
		"Precipitation",
		"Snow",
		"Humidity",
		"GeneralMetaData",
		"Cloud",
		"Pressure",
		"Temperature"
	) NOT NULL UNIQUE
);

CREATE TABLE barometricDescription(
	idBarometricDescription Int Auto_Increment NOT NULL Primary Key,
	barometricDescriptionCode Int NOT NULL UNIQUE,
	barometricDescription VARCHAR(255) NOT NULL
);

CREATE TABLE parametersDescription(
	idParametersDescription Int Auto_Increment NOT NULL Primary Key,
	textualDescription VARCHAR(255) NOT NULL
);

CREATE TABLE weatherDescription(
	idWeatherDescription Int Auto_Increment NOT NULL Primary Key,
	weatherDescriptionCode Int NOT NULL UNIQUE,
	textualDescription VARCHAR(255) NOT NULL
);

CREATE TABLE region(
	idRegion Int Auto_Increment NOT NULL Primary Key,
	regionName VARCHAR(255) NOT NULL
);

CREATE TABLE neighboursRegion(
	idRegion1 INT NOT NULL,
	idRegion2 INT NOT NULL,
	Primary Key (idRegion1, idRegion2)
);

CREATE TABLE station(
	idStation Int Auto_Increment NOT NULL Primary Key,
	stationCode Char(5) NOT NULL UNIQUE,
	stationName VARCHAR(255) NOT NULL,
	lat Decimal(10, 2) NOT NULL,
	lng Decimal(10, 2) NOT NULL,
	altitude INT NOT NULL,
	idRegion Int,
	FOREIGN KEY (idRegion) REFERENCES region(idRegion)
);

-- Precipitation Data
CREATE TABLE lastNHoursPrecipitation(
	idLastNHoursPrecipitation Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	timeInterval INT NOT NULL DEFAULT 1,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit),
	CHECK (timeInterval IN (1, 3, 6, 12, 24))
);

CREATE TABLE precipitationData(
	idPrecipitationData Int Auto_Increment NOT NULL Primary Key,
	idLast1HoursPrecipitation Int,
	idLast3HoursPrecipitation Int,
	idLast6HoursPrecipitation Int,
	idLast12HoursPrecipitation Int,
	idLast24HoursPrecipitation Int,
	FOREIGN KEY (idLast1HoursPrecipitation) REFERENCES lastNHoursPrecipitation(idLastNHoursPrecipitation),
	FOREIGN KEY (idLast3HoursPrecipitation) REFERENCES lastNHoursPrecipitation(idLastNHoursPrecipitation),
	FOREIGN KEY (idLast6HoursPrecipitation) REFERENCES lastNHoursPrecipitation(idLastNHoursPrecipitation),
	FOREIGN KEY (idLast12HoursPrecipitation) REFERENCES lastNHoursPrecipitation(idLastNHoursPrecipitation),
	FOREIGN KEY (idLast24HoursPrecipitation) REFERENCES lastNHoursPrecipitation(idLastNHoursPrecipitation)
);

-- Wind Data
CREATE TABLE windDirectionDescription(
	idWindDirectionDescription Int Auto_Increment NOT NULL Primary Key,
	minValue DECIMAL(5, 2) NOT NULL,
	mxValue DECIMAL(5, 2) NOT NULL,
	textualDescription VARCHAR(255) NOT NULL,
	CHECK (minValue <= mxValue)
);

CREATE TABLE windSpeedDescription(
	idWindSpeedDescription Int Auto_Increment NOT NULL Primary Key,
	minValue Decimal(6, 2) NOT NULL,
	mxValue Decimal(6, 2) NOT NULL,
	CHECK (minValue <= mxValue),
	textualDescription VARCHAR(255) NOT NULL,
	observation VARCHAR(255) NOT NULL
);

CREATE TABLE averageWindSpeed(
	idAverageWindSpeed Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	idWindSpeedDescription INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit),
	FOREIGN KEY (idWindSpeedDescription) REFERENCES windSpeedDescription(idWindSpeedDescription)
);

CREATE TABLE averageWindDirection(
	idAverageWindDirection Int Auto_Increment NOT NULL Primary Key,
	paramValue INT,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	idWindDirectionDescription INT,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit),
	FOREIGN KEY (idWindDirectionDescription) REFERENCES windDirectionDescription(idWindDirectionDescription)
);

CREATE TABLE windGustMeasurementPeriod(
	idWindGustMeasurementPeriod Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2) NOT NULL,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE windGustOnAPeriod(
	idWindGustOnAPeriod Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE windData(
	idWindData Int Auto_Increment NOT NULL Primary Key,
	idAverageWindSpeed Int,
	idAverageWindDirection Int,
	idWindGustMeasurementPeriod Int,
	idWindGustOnAPeriod Int,
	FOREIGN KEY (idAverageWindSpeed) REFERENCES averageWindSpeed(idAverageWindSpeed),
	FOREIGN KEY (idAverageWindDirection) REFERENCES averageWindDirection(idAverageWindDirection),
	FOREIGN KEY (idWindGustMeasurementPeriod) REFERENCES windGustMeasurementPeriod(idWindGustMeasurementPeriod),
	FOREIGN KEY (idWindGustOnAPeriod) REFERENCES windGustOnAPeriod(idWindGustOnAPeriod)
);

-- GeneralData
CREATE TABLE horizontalVisibility(
	idHorizontalVisibility Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE dewPoint(
	idDewPoint Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE generalData(
	idGeneralData Int Auto_Increment NOT NULL Primary Key,
	idHorizontalVisibility Int,
	idDewPoint Int,
	FOREIGN KEY (idHorizontalVisibility) REFERENCES horizontalVisibility(idHorizontalVisibility),
	FOREIGN KEY (idDewPoint) REFERENCES dewPoint(idDewPoint)
);

-- Snow
CREATE TABLE snowHeight(
	idSnowHeight Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE snowData(
	idSnowData Int Auto_Increment NOT NULL Primary Key,
	idSnowHeight Int,
	FOREIGN KEY (idSnowHeight) REFERENCES snowHeight(idSnowHeight)
);

-- Humidity
CREATE TABLE humidity(
	idHumidity Int Auto_Increment NOT NULL Primary Key,
	paramValue INT,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE humidityData(
	idHumidityData Int Auto_Increment NOT NULL Primary Key,
	idHumidity Int,
	FOREIGN KEY (idHumidity) REFERENCES humidity(idHumidity)
);

-- Cloud
CREATE TABLE lowCloudLevel(
	idLowCloudLevel Int Auto_Increment NOT NULL Primary Key,
	paramValue INT,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE cloudData(
	idCloudData Int Auto_Increment NOT NULL Primary Key,
	idLowCloudLevel Int,
	FOREIGN KEY (idLowCloudLevel) REFERENCES lowCloudLevel(idLowCloudLevel)
);

-- Pressure
CREATE TABLE nHoursPressureVariation(
	idNHoursPressureVariation Int Auto_Increment NOT NULL Primary Key,
	paramValue INT,
	timeInterval INT NOT NULL DEFAULT 3,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit),
	CHECK (timeInterval IN (3, 24))
);

CREATE TABLE stationPressure(
	idStationPressure Int Auto_Increment NOT NULL Primary Key,
	paramValue INT,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE seaLevelPressure(
	idSeaLevelPressure Int Auto_Increment NOT NULL Primary Key,
	paramValue INT,
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE pressureData(
	idPressureData Int Auto_Increment NOT NULL Primary Key,
	id1HoursPressureVariation Int,
	id24HoursPressureVariation Int,
	idStationPressure Int,
	idSeaLevelPressure Int,
	FOREIGN KEY (id1HoursPressureVariation) REFERENCES nHoursPressureVariation(idNHoursPressureVariation),
	FOREIGN KEY (id24HoursPressureVariation) REFERENCES nHoursPressureVariation(idNHoursPressureVariation),
	FOREIGN KEY (idStationPressure) REFERENCES stationPressure(idStationPressure),
	FOREIGN KEY (idSeaLevelPressure) REFERENCES seaLevelPressure(idSeaLevelPressure)
);

-- Temperature
CREATE TABLE temperature(
	idTemperature Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE groundMinTemperature(
	idGroundMinTemperature Int Auto_Increment NOT NULL Primary Key,
	paramValue Decimal(5, 2),
	idUniverse INT NOT NULL,
	idUnit INT NOT NULL,
	FOREIGN KEY (idUniverse) REFERENCES universe(idUniverse),
	FOREIGN KEY (idUnit) REFERENCES unit(idUnit)
);

CREATE TABLE temperatureData(
	idTemperatureData Int Auto_Increment NOT NULL Primary Key,
	idTemperature Int,
	idGroundMinTemperature Int,
	FOREIGN KEY (idTemperature) REFERENCES temperature(idTemperature),
	FOREIGN KEY (idGroundMinTemperature) REFERENCES groundMinTemperature(idGroundMinTemperature)
);

-- Add the FK on entryPoint now these are created
CREATE TABLE entryPoint(
	idEntryPoint Int Auto_Increment NOT NULL Primary Key,
	dateEntryPoint DATETIME NOT NULL,
	idStation INT NOT NULL,
	idPrecipitationData INT NOT NULL,
	idWindData INT NOT NULL,
	idGeneralData INT NOT NULL,
	idSnowData INT NOT NULL,
	idHumidityData INT NOT NULL,
	idCloudData INT NOT NULL,
	idPressureData INT NOT NULL,
	idTemperatureData INT NOT NULL,
	idWeatherDescription INT NOT NULL,
	idBarometricDescription INT NOT NULL,
	FOREIGN KEY (idStation) REFERENCES station(idStation),
	FOREIGN KEY (idPrecipitationData) REFERENCES precipitationData(idPrecipitationData),
	FOREIGN KEY (idWindData) REFERENCES windData(idWindData),
	FOREIGN KEY (idGeneralData) REFERENCES generalData(idGeneralData),
	FOREIGN KEY (idSnowData) REFERENCES snowData(idSnowData),
	FOREIGN KEY (idHumidityData) REFERENCES humidityData(idHumidityData),
	FOREIGN KEY (idCloudData) REFERENCES cloudData(idCloudData),
	FOREIGN KEY (idPressureData) REFERENCES pressureData(idPressureData),
	FOREIGN KEY (idTemperatureData) REFERENCES temperatureData(idTemperatureData),
	FOREIGN KEY (idWeatherDescription) REFERENCES weatherDescription(idWeatherDescription),
	FOREIGN KEY (idBarometricDescription) REFERENCES barometricDescription(idBarometricDescription),
	UNIQUE(dateEntryPoint, idStation)
);